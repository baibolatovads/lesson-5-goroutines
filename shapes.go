package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"

	//"io/ioutil"
	"math"
	"math/rand"
	"reflect"
	"strconv"
	"time"
)

//interface with Area() function
type myShape interface {
	Area() float64
}

type Shape struct{
	Type string `json:"type"`
	Radius interface{} `json:"radius, omitempty"`
	Height interface{} `json:"height, omitempty"`
	SideA interface{} `json:"a, omitempty"`
	SideB interface{} `json:"b, omitempty"`
	SideC interface{} `json:"c, omitempty"`
}

type Circle struct{
	Radius float64
}

//S = πR^2
func (c * Circle) Area() float64{
	return math.Pi * c.Radius * c.Radius
}

type Rectangle struct{
	A float64
	B float64
}

//S = ab
func (r * Rectangle) Area() float64{
	return r.A * r.B
}

type Triangle struct{
	Side float64
	Height float64
}

//S = 1/2*A*H
func (t * Triangle) Area() float64{
	return 0.5 * t.Side * t.Height
}

//function to use reflection
func ConvertToFloat64 (i interface{}) (float64, error){
	reflectType := reflect.TypeOf(i)
	reflectValue := reflect.ValueOf(i)
	switch reflectType.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return float64(reflectValue.Int()), nil
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return float64(reflectValue.Uint()), nil
	case reflect.Float32, reflect.Float64:
		return reflectValue.Float(), nil
	case reflect.String:
		val, err := strconv.ParseFloat(reflectValue.String(), 64)
		if err != nil {
			panic(err.Error())
		}
		return val, nil
	default:
		panic(errors.New("Incorrect data provided!"))
	}
}

//Function to calculate area using reflection
func (shape Shape) Area() float64 {
	switch shape.Type {
	case "circle":
		Radius, err := ConvertToFloat64(shape.Radius)
		if err != nil {
			return 0
		}
		circle := Circle{Radius: Radius}
		return circle.Area()
	case "rectangle":
		sideA, err := ConvertToFloat64(shape.SideA)
		if err != nil {
			return 0
		}
		sideB, err := ConvertToFloat64(shape.SideB)
		if err != nil {
			return 0
		}
		rec := Rectangle{A: sideA, B: sideB}
		return rec.Area()
	case "triangle":
		sideA, err := ConvertToFloat64(shape.SideA)
		if err != nil {
			return 0
		}
		Height, err := ConvertToFloat64(shape.Height)
		if err != nil {
			return 0
		}
		triangle := Triangle{Side: sideA, Height: Height}
		return triangle.Area()

		default:
			return 0
	}
}

//Function to generate sides for the shape
func GenerateSides() []interface{} {
	var resp []interface{}

	resp = append(resp, 1 + rand.Intn(1000))
	resp = append(resp,1 + rand.Float32())
	resp = append(resp, strconv.Itoa(1 + rand.Intn(1000)))
	resp = append(resp, fmt.Sprintf("%f", 1 + rand.Float32()))

	return resp
}

//returns new shape
func GiveMeShape() Shape{
		shape := Shape{}
		shapeNum := rand.Intn(4)
		switch shapeNum {
		case 0:
			shape.Type = "circle"
			radiusType := rand.Intn(4)
			shape.Radius = GenerateSides()[radiusType]
		case 1:
			shape.Type = "rectangle"
			sideAType := rand.Intn(4)
			sideBType := rand.Intn(4)
			shape.SideA = GenerateSides()[sideAType]
			shape.SideB = GenerateSides()[sideBType]
		case 3:
			shape.Type = "triangle"
			sideAType := rand.Intn(4)
			height := rand.Intn(4)
			shape.SideA = GenerateSides()[sideAType]
			shape.Height = GenerateSides()[height]
		}
		return shape
}
func main() {
	//indicates the start of algorithm
	start := time.Now()

	//First type of generating dataset
	var shapes []Shape
	var shape Shape
	for i:=0; i < 10000; i++{
		shape = GiveMeShape()
		shapes = append(shapes, shape)
	}
	shapesJson, err := json.Marshal(shapes)
		if err != nil {
			panic(err.Error())
		}
	err = ioutil.WriteFile("test.json", shapesJson, 0666)
		if err != nil {
			panic(err.Error())
		}
		fmt.Println("OK")


	//Second type of generating dataset
	//myShape := [3]string{"circle", "triangle", "rectangle"}
	//var data []Shape
	//for i:=0; i < 10000; i++{
	//	r1 := myShape[rand.Intn(len(myShape))]
	//	if r1 == "circle" {
	//		r := rand.Intn(5)
	//		a := Shape{Type:r1, Radius:r}
	//		data = append(data, a)
	//	}
	//	if r1 == "triangle"{
	//		sideA := rand.Intn(5)
	//		H := rand.Intn(5)
	//		a := Shape{Type:r1, SideA:sideA, Height:H}
	//		data = append(data, a)
	//	}
	//	if r1 == "rectangle" {
	//		sideA := rand.Intn(5)
	//		sideB := rand.Intn(5)
	//		a := Shape{Type: r1, SideA: sideA, SideB: sideB}
	//		data = append(data, a)
	//	}
	//}
	//
	//dataJson, err := json.Marshal(data)
	//if err != nil {
	//	log.Fatal("Cannot encode to JSON ", err)
	//}
	////fmt.Fprintf(os.Stdout, "%s", dataJson)
	//_ = ioutil.WriteFile("test.json", dataJson, 0644)
	//myFile, _ := ioutil.ReadFile("test.json")
	//fmt.Println("OK")
	//shapes := []Shape{}
	//_ = json.Unmarshal(myFile, &shapes)



	//Without using goroutines:
	//for _, shape := range shapes {
	//	area := shape.Area()
	//	shapeJson, _ := json.Marshal(shape)
	//	fmt.Println(string(shapeJson), area)
	//	}


	//creating channel
	doneChan := make(chan float64, 10000)
	done := make(chan bool)
	go func() {
		for {
			j, more := <-doneChan
			if more {
				fmt.Println("received area", j)
			} else {
				fmt.Println("received all areas")
				done <- true
				return
			}
		}
	}()

		for _, shape := range shapes {
			area := shape.Area()
			doneChan <- area
			fmt.Println("Area: ", area)
		}
		close(doneChan)
		fmt.Println("Sent all areas")
		<-done
	t := time.Now()
	elapsed := t.Sub(start)
	fmt.Println(elapsed)
	}

